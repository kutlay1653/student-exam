import java.util.ArrayList;
import java.util.Scanner;


public class Big_Class {

    public static void main(String[] args) {

        ArrayList<Student> AA = new ArrayList<>();              // AA = 100-90
        ArrayList<Student> BB = new ArrayList<>();              // BB = 90 - 70
        ArrayList<Student> CC = new ArrayList<>();              // CC = 70-60
        ArrayList<Student> DD = new ArrayList<>();              // DD = 60-50
        ArrayList<Student> EE = new ArrayList<>();              // EE = 50-30
        ArrayList<Student> FF = new ArrayList<>();              // FF = 30-0

        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("What is the number of Student in your class?: ");
                int numberOfStudent = Integer.valueOf(sc.nextLine());


                for (int i = 0; i < numberOfStudent; i++) {

                    Student new_student = new Student();

                    System.out.println("What's " + (i + 1) + ". student's name? ");
                    String incomingName = sc.nextLine();
                    new_student.setName(incomingName);

                    System.out.println("What's " + (i + 1) + ". student's surname? ");
                    String incomingSurname = sc.nextLine();
                    new_student.setSurname(incomingSurname);

                    while (true) {

                        System.out.println("What's " + (i + 1) + ". student's exam1? ");
                        int incomingExam1 = Integer.valueOf(sc.nextLine());
                        if (incomingExam1 >= 0 && incomingExam1 <= 100) {
                            new_student.setExam1(incomingExam1);
                            break;

                        } else {

                            System.out.println("Insert between 0-100");

                        }

                    }
                    while (true) {

                        System.out.println("What's " + (i + 1) + ". student's exam2? ");
                        int incomingExam2 = Integer.valueOf(sc.nextLine());
                        if (incomingExam2 >= 0 && incomingExam2 <= 100) {
                            new_student.setExam2(incomingExam2);
                            break;

                        } else {

                            System.out.println("Insert between 0-100");

                        }
                    }

                    while (true) {

                        System.out.println("What's " + (i + 1) + ". student's exam3? ");
                        int incomingExam3 = Integer.valueOf(sc.nextLine());
                        if (incomingExam3 >= 0 && incomingExam3 <= 100) {
                            new_student.setExam3(incomingExam3);
                            break;

                        } else {

                            System.out.println("Insert between 0-100");

                        }
                    }


                    if (new_student.getAverage() >= 90) {

                        AA.add(new_student);
                    } else if (new_student.getAverage() < 90 && new_student.getAverage() >= 70) {

                        BB.add(new_student);
                    } else if (new_student.getAverage() < 70 && new_student.getAverage() >= 60) {

                        CC.add(new_student);
                    } else if (new_student.getAverage() < 60 && new_student.getAverage() >= 50) {

                        DD.add(new_student);
                    } else if (new_student.getAverage() < 50 && new_student.getAverage() >= 40) {

                        EE.add(new_student);
                    } else {

                        FF.add(new_student);
                    }

                }
                while (true) {
                    try {
                        System.out.print("\nPlease insert a number between 0 - 6: \n to list AA:1 \n to list BB=2 \n to list CC=3 \n to list DD=4 \n to list EE=5\n FF=6 \n to list Exit=0\n What would you like to do?\n");
                        int input = Integer.valueOf(sc.nextLine());

                        if (input == 1) {

                            System.out.println("-------Student which has got AA : " + AA.size() + "-------");

                        } else if (input == 2) {

                            System.out.println("-------Student which has got BB : " + BB.size() + "-------");

                        } else if (input == 3) {

                            System.out.println("-------Student which has got CC : " + CC.size() + "-------");

                        } else if (input == 4) {

                            System.out.println("-------Student which has got DD : " + DD.size() + "-------");

                        } else if (input == 5) {

                            System.out.println("-------Student which has got EE : " + EE.size() + "-------");

                        } else if (input == 6) {

                            System.out.println("-------Student which has got FF : " + FF.size() + "-------");

                        } else if (input == 0) {

                            break;

                        }
                    } catch (Exception e) {

                        System.out.print("Please insert a number between 0-6: ");
                    }
                }
                break;
            } catch (Exception e) {
                System.out.print("Please insert a number of student in the class..");

            }
        }

    }


}
