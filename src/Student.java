public class Student {

    String name;
    String surname;
    int exam1;
    int exam2;
    int exam3;
    int average;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getExam1() {
        return exam1;
    }

    public void setExam1(int exam1) {
        this.exam1 = exam1;
    }

    public int getExam2() {
        return exam2;
    }

    public void setExam2(int exam2) {
        this.exam2 = exam2;
    }

    public int getExam3() {
        return exam3;
    }

    public void setExam3(int exam3) {
        this.exam3 = exam3;
    }

    public int getAverage() {
        return (this.exam1 + this.exam2 + this.exam3)/3;
    }

    public void setAverage() {
        this.average = average ;
    }
}
